//
//  InterfaceController.swift
//  applewatchi18n WatchKit Extension
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

  @IBOutlet weak var timeLabel: WKInterfaceLabel!
  @IBOutlet weak var greetingLabel: WKInterfaceLabel!
  
  override func awakeWithContext(context: AnyObject?) {
    super.awakeWithContext(context)
  }
  
  override func willActivate() {
    super.willActivate()
  }
  
  override func didDeactivate() {
    super.didDeactivate()
  }
  
}
